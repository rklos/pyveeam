from setuptools import setup

setup(
    install_requires=[
        'requests',
        'xmltodict'
    ],
    name='veeamclient',
    version='1.1',
    py_modules=['veeamclient']
)
