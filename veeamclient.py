import xml.etree.ElementTree as ET
from pprint import pprint as pp
from base64 import b64decode
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import xmltodict
import re

# Suppress InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class VeeamSession(object):
    
    def __init__(self, **args):
        self.api_hostname = args.get('hostname', 'localhost')
        self.api_username = args.get('username')
        self.api_password = args.get('password')
        self.api_ssl = args.get('use_tls', False)
        self.api_verify_ssl = args.get('verify_tls', True)
        self.api_namespace = {
            'veeam': args.get(
                'namespace',
                'http://www.veeam.com/ent/v1.0'
            )
        }

        # Shortcut to namespace
        self._ns = self.api_namespace

        if self.api_ssl:
            self.api_port = args.get('port', 9398)
            self.api_url = 'https://{hostname}:{port}/api/'.format(
                hostname=self.api_hostname,
                port=self.api_port
            )
        else:
            self.api_port = args.get('port', 9399)
            self.api_url = 'http://{hostname}:{port}/api/'.format(
                hostname=self.api_hostname,
                port=self.api_port
            )

        self.api_session_headers = {
            'Content-type': 'text/xml',
            'X-Requested-With': 'VeeamClient'
        }

        self.api_auth_url = self._get_auth_url()

        (self.session_id, self.session_id_plain) = self._login()


    def _get_auth_url(self):
        result = requests.get(
            self.api_url,
            headers=self.api_session_headers,
            verify=self.api_verify_ssl
        )

        xml_result = ET.fromstring(result.text)
        links = xml_result.find('veeam:Links', self._ns)
        for link in links:
            if link.attrib.get('Rel') == 'Create':
                return link.attrib.get('Href')
        else:
            raise StandardError('Could not find Create URL from API')


    def _login(self):
        auth_data = (self.api_username, self.api_password)

        result = requests.post(
            self.api_auth_url,
            auth=auth_data,
            headers=self.api_session_headers,
            verify=self.api_verify_ssl
        )

        if result.status_code != 201:
            raise Exception('Could not login: {error}'.format(
                error=str(result.text)
            ))

        session_id = result.headers['X-RestSvcSessionId']
        session_id_plain = b64decode(session_id).decode("utf-8") 

        self.api_session_headers['X-RestSvcSessionId'] = session_id

        return session_id, session_id_plain

    def _logout(self):
        result = self.delete_path('/logonSessions/{session_id}'.format(
            session_id=self.session_id_plain
        ))
        
        if result.status_code != 204:
            raise Exception('Could not logout: {error}'.format(
                error=str(result.text)
            ))
        
        return True

    def delete_path(self, path):
        req_url = '{url}/{path}'.format(
            url=self.api_url,
            path=path
        )

        result = requests.delete(
            req_url,
            headers=self.api_session_headers,
            verify=self.api_verify_ssl
        )

        return result

    def post_path(self, path, payload=None):
        req_url = '{url}/{path}'.format(
            url=self.api_url,
            path=path
        )

        result = requests.post(
            req_url,
            headers=self.api_session_headers,
            verify=self.api_verify_ssl,
            data=payload
        )

        return result


    def get_path(self, path):
        req_url = '{url}/{path}'.format(
            url=self.api_url,
            path=path
        )

        result = requests.get(
            req_url,
            headers=self.api_session_headers,
            verify=self.api_verify_ssl
        )

        return result


    def _check_login(self):
        result = self.get_path('/logonSessions/{session_id}'.format(
            session_id=self.session_id_plain
        ))
        
        if result.status_code == 401:
            return False
        
        return True


    def get_logonSessions(self):
        result = self.get_path('/logonSessions')
        return ET.fromstring(result.text)


    def get_logonSession(self, session_id):
        result = self.get_path('/logonSessions/{id}'.format(
            id=session_id
        ))
        return ET.fromstring(result.text)


    def get_capabilities(self):
        session = self.get_logonSession(self.session_id_plain)
        links = session.find('veeam:Links', self._ns)
        return links


    @property
    def logged_in(self):
        return self._check_login()
    

    @property
    def logonSessions(self):
        return self.get_logonSessions()


    @property
    def logonSession(self):
        return self.get_logonSession(self.session_id_plain)


    @property
    def namespace(self):
        return self.api_namespace

    @namespace.setter
    def namespace(self, namespace={}):
        self.api_namespace = namespace
        self._ns = self.api_namespace


    @property
    def logon_paths(self):
        links = self.get_capabilities()
        paths = []

        for link in links:
            url = link.attrib.get('Href')
            paths.append(
                '/{path}'.format(
                    path=url.split(self.api_url, 2)[-1]
                )
            )
        return paths


class BaseVeeam(object):

    def __init__(self, session):
        if session.logged_in:
            self._s = session
        else:
            raise StandardError('Must log in with VeeamSession first')


"""
API Path: /reports/

Takes a VeeamSession instance as argument.
"""
class VeeamReports(BaseVeeam):

    def _get_summary_job_statistics(self):
        statistics = self._s.get_path('/reports/summary/job_statistics')
        ns = {'http://www.veeam.com/ent/v1.0':None}
        return dict(xmltodict.parse(statistics.text,process_namespaces=True,namespaces=ns)['JobStatisticsReportFrame'])


    def _get_summary_overview(self):
        overview = self._s.get_path('/reports/summary/overview')
        #return ET.fromstring(overview.text)
        ns = {'http://www.veeam.com/ent/v1.0':None}
        return dict(xmltodict.parse(overview.text,process_namespaces=True,namespaces=ns)['OverviewReportFrame'])

    def _get_processed_vms(self):
        overview = self._s.get_path('/reports/summary/processed_vms')
        ns = {'http://www.veeam.com/ent/v1.0':None}
        return dict(xmltodict.parse(overview.text,process_namespaces=True,namespaces=ns)['ProcessedVmsReportFrame'])

    def _get_vm_overview(self):
        overview = self._s.get_path('/reports/summary/vms_overview')
        ns = {'http://www.veeam.com/ent/v1.0':None}
        return dict(xmltodict.parse(overview.text,process_namespaces=True,namespaces=ns)['VmsOverviewReportFrame'])

    def _get_repository_statistics(self):
        overview = self._s.get_path('/reports/summary/repository')
        xmlstring = re.sub(' xmlns="[^"]+"', '', overview.text, count=1)
        root =  ET.fromstring(xmlstring)
        repos = []
        for child in root.findall('Period'):
            resource = {'name': child.find('Name').text,'capacity': child.find('Capacity').text,'free':child.find('FreeSpace').text,'backupsize':child.find('BackupSize').text}
            repos.append(resource)
        return(repos)

    @property
    def job_statistics(self):
        return self._get_summary_job_statistics()

"""
API Path: /cloud/

Takes a VeeamSession instance as argument.
"""
class VeeamCloud(BaseVeeam):

    def _get_tenant_list(self):
        tlist = []
        tenants = self._s.get_path('/cloud/tenants')
        for tenant in ET.fromstring(tenants.text):
            tlist.append({'Name':tenant.attrib.get('Name'),'Id':tenant.attrib.get('UID').split(':')[3]})
        return tlist

    def _get_tenant(self,id):
        import re
        res = {}
        resources = self._s.get_path('/cloud/tenants/{}?format=Entity'.format(id))
        xmlstring = re.sub(' xmlns="[^"]+"', '', resources.text, count=1)
        root =  ET.fromstring(xmlstring)
        res['Enabled'] = bool(root.find('Enabled').text=='true')
        res['Backups'] = root.find('BackupCount').text
        res['LastResult'] = root.find('LastResult').text
        res['LastActive'] = root.find('LastActive').text
        res['ThrottlingEnabled'] = bool(root.find('ThrottlingEnabled').text=='true')
        res['ThrottlingSpeedLimit'] = root.find('ThrottlingSpeedLimit').text
        res['ThrottlingSpeedUnit'] = root.find('ThrottlingSpeedUnit').text
        res['PublicIpCount'] = root.find('PublicIpCount').text
        res['BackupCount'] = root.find('BackupCount').text
        res['ReplicaCount'] = root.find('ReplicaCount').text
        res['MaxConcurrentTasks'] = root.find('MaxConcurrentTasks').text
        #res['WorkStationBackupCount'] = root.find('WorkStationBackupCount').text
        #res['ServerBackupCount'] = root.find('ServerBackupCount').text

        return res

    def _create_tenant(self):
        '<?xml version="1.0" encoding="utf-8"?>\
        <CreateCloudTenantSpec xmlns="http://www.veeam.com/ent/v1.0" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\
        <Name>ABC Company<Name>\
        <Description>Tenant account for ABC Company</Description>\
        <Password>1234</Password>\
        <Enabled>true</Enabled>\
        <LeaseExpirationDate>2099-12-31T00:00:00</LeaseExpirationDate>\
        <Resources>\
        <BackupResource>\
        <Name>Cloud Repository</Name>\
        <RepositoryUid>urn:veeam:Repository:a0f35f34-8d58-4470-b52d-071e1417732a\
        </RepositoryUid>\
        <QuotaMb>307200</QuotaMb>\
        </BackupResource>\
        </Resources>\
        <ThrottlingEnabled>false</ThrottlingEnabled>\
        <ThrottlingSpeedLimit>1</ThrottlingSpeedLimit>\
        <ThrottlingSpeedUnit>MBps</ThrottlingSpeedUnit>\
        <PublicIpCount>0</PublicIpCount>\
        <BackupServerUid>urn:veeam:BackupServer:8fff3b8e-c3f1-4ef5-aecc-561f07bf9982</BackupServerUid>\
        <MaxConcurrentTasks>2</MaxConcurrentTasks>\
        </CreateCloudTenantSpec>'

    def _get_tenant_resources(self,id):
        import re
        res = []
        resources = self._s.get_path('/cloud/tenants/{}/resources'.format(id))
        xmlstring = re.sub(' xmlns="[^"]+"', '', resources.text, count=1)
        root =  ET.fromstring(xmlstring)
        
        for quota in root.findall('CloudTenantResource'):
            child  = quota.find('RepositoryQuota')
            resource = {'Name': child.find('DisplayName').text,'Quota': child.find('Quota').text,'Used':child.find('UsedQuota').text, 'RepoId':child.find('RepositoryUid').text}
            res.append(resource)

        return res

    def _set_tenant_resources(self,tenant_id, repo_id):
        #Not yet implemented
        res = []
        resources = self._s.get_path('/cloud/tenants/{}/resources'.format(id))
        xmlstring = re.sub(' xmlns="[^"]+"', '', resources.text, count=1)
        root =  ET.fromstring(xmlstring)
        
        for quota in root.findall('CloudTenantResource'):
            child  = quota.find('RepositoryQuota')
            resource = {'name': child.find('DisplayName').text,'quota': child.find('Quota').text,'used':child.find('UsedQuota').text}
            res.append(resource)

        return res


    def _get_summary_overview(self):
        overview = self._s.get_path('/reports/summary/overview')
        return ET.fromstring(overview.text)

    def _get_repo_list(self):
        rlist = []
        resources = self._s.get_path('/repositories')
        #print(resources.text)
        for resource in ET.fromstring(resources.text):
            rlist.append({'Name':resource.attrib.get('Name'),'Id':resource.attrib.get('UID').split(':')[3]})
        return rlist

    def _get_repo(self,id):
        import re
        res = {}
        resources = self._s.get_path('/repositories/{}?format=Entity'.format(id))
        xmlstring = re.sub(' xmlns="[^"]+"', '', resources.text, count=1)
        #print(xmlstring)
        root =  ET.fromstring(xmlstring)
        res['Capacity'] = root.find('Capacity').text
        res['FreeSpace'] = root.find('FreeSpace').text
        res['Kind'] = root.find('Kind').text

        return res

    @property
    def job_statistics(self):
        return self._get_summary_job_statistics()
